import './style.css'
import * as THREE from 'three'
import * as dat from 'lil-gui'
import gsap from 'gsap'
import { OrbitControls } from '../static/gizmo/OrbitControls.js'
import { OrbitControlsGizmo } from '../static/gizmo/OrbitControlsGizmo.js'
import { Material } from 'three'
import { GLTFLoader } from 'three/examples/jsm/loaders/GLTFLoader.js'
import { DRACOLoader } from 'three/examples/jsm/loaders/DRACOLoader.js'

/**
 * Components
 */
const component = {
    arrow: 0x9e3aea,
    hanger: 0x3adfea,
    shadow_caster: 0xea3a98,
    hinge: 0x86ea3a,
    top_part: 0xea463a,
    bottom_part: 0xeae43a,
    compass_rose: 0xeab83a,
    sun: 0xfafd0f,
    compass_glass: 0x000000
}

// model animation parameters
const modelAnimationParams = {
    compassHanger: 0,
    cmpassHinge: 0,
    gnomonicGroupe: 0,
    rotationSpeed: 0.01,
    isAnimating: false
  };

/**
 * Animation calculation
 */
const animations = {

    // sun circles around the origin along y-axis
    bornInTheUSA: () => 
    {
        gsap.killTweensOf(pointLight.rotation);
        gsap.to(pointLight.rotation, {
            duration: 20,
            y: Math.PI * 4,
            repeat: -1,
            ease: 'none',
            onUpdate: () => {
                const timeline = gsap.timeline();
                
                // Update the position of the cube based on its rotation
                const angle = pointLight.rotation.y;
                const x = Math.cos(angle) * 7;
                const y = pointLight.position.y;
                const z = Math.sin(angle) * 7;

                timeline.to(pointLight.position, { 
                    duration: 0.01,
                    x: x,
                    y: y,
                    z: z
                });
            }
        })                       
    },

    californaDreamin: () =>
    {
        gsap.killTweensOf(pointLight.rotation);
        gsap.to(pointLight.rotation, {
            duration: 20,
            y: Math.PI * 2,
            repeat: -1,
            ease: 'none',
            onUpdate: () => {
                const timeline = gsap.timeline();
                
                const angle = pointLight.rotation.y;
                const x = Math.sin(angle) * 7;
                const y = Math.cos(angle) * 7;
                const z = Math.sin(angle) * 7;

                timeline.to(pointLight.position, { 
                    duration: 0.01,
                    x: x,
                    y: y,
                    z: z
                });
            }
        })                              
    },
}


/**
 * html & css components
 */

// progress bar for loading model 
let loadingBarContainer = document.getElementById('loading-bar-container');
function updateLoadingProgress(percent) {
    const loadingBar = document.getElementById('loading-bar');
    loadingBar.style.width = `${percent}%`;
    loadingBar.textContent = `${Math.round(percent)}%`;
}

// play button for activating animation
//let inspectModelButton = document.getElementById('inspectModelButton');
let sunMovingButton = document.getElementById('sunMovingButton');
let playOpenAnimationButton = document.getElementById('playOpenAnimationButton');
 

/**
 * Base
 */
// Canvas
const canvas = document.querySelector('canvas')
// Scene
const scene = new THREE.Scene()
//scene.background = new THREE.Color( 0x003B00 );


// Instantiate the darco compressed loader
const dracoLoader = new DRACOLoader()
// set decoder path
dracoLoader.setDecoderPath('/draco/')
// Instantiate the glTF loader
const gltfLoader = new GLTFLoader();
// setup dracoloader
gltfLoader.setDRACOLoader(dracoLoader) 

// Load compressed glTF resource
let glbAnimations = [];
let mixer;
gltfLoader.load(
    //'/models/draco_remake/remake_final.glb',
    '/animations/how_to_open.glb',
    (gltf) =>
    {
        const model = gltf.scene;
        model.scale.set(2, 2, 2);
        scene.add(model)

        // Tilt the model by 35 degrees on the X-axis
        model.rotation.z = -35 * (Math.PI / 180);
        console.log();


        mixer = new THREE.AnimationMixer(model);
        glbAnimations = gltf.animations;

        // tun on animation for components
        model.traverse((child) => {
            if (child.isMesh) {
                // Enable shadow-related properties
                if (child.name) {
                    //console.log('Found mesh:', child.name);
                }

                // Enable shadow casting for specific target
                if (child.name === 'ShadowcasterOpen') {
                    child.castShadow = true;
                    //console.log(`${child.name}.castShadow = true`);
                }

                // Enable shadow receiving for a target part
                if (child.name === 'gnomonic_table_mesh') {
                    child.receiveShadow = true;
                    //console.log(`${child.name}.receiveShadow = true`);
                }

                // Enable shadow receiving for a target part
                if (child.name === 'gnomonic_table_mesh_1') {
                    child.receiveShadow = true;
                    //console.log(`${child.name}.receiveShadow = true`);
                }
            }
        });


        const compassHangerClip = THREE.AnimationClip.findByName(glbAnimations, 'CompassHangerOpen')
        const compassHangerAction = mixer.clipAction(compassHangerClip)
        compassHangerAction.setLoop(THREE.LoopOnce); 
        compassHangerAction.clampWhenFinished = true; 

        const gnomonicTableClip = THREE.AnimationClip.findByName(glbAnimations, 'GnomonicTableOpen')
        const gnomonicTableAction = mixer.clipAction(gnomonicTableClip)
        gnomonicTableAction.setLoop(THREE.LoopOnce);
        gnomonicTableAction.clampWhenFinished = true; 

        const shadowCasterClip = THREE.AnimationClip.findByName(glbAnimations, 'ShadowcasterOpen')
        const shadowCasterAction = mixer.clipAction(shadowCasterClip)
        shadowCasterAction.setLoop(THREE.LoopOnce);
        shadowCasterAction.clampWhenFinished = true; 


        // Function to play all animation actions
        function playOpenAnimation() {
            const timeScaleValue = 100;
            compassHangerAction.timeScale = timeScaleValue;
            gnomonicTableAction.timeScale = timeScaleValue;
            shadowCasterAction.timeScale = timeScaleValue;
            compassHangerAction.reset().play();
            gnomonicTableAction.reset().play();
            shadowCasterAction.reset().play();
        }

        // Add click event listener to the play button
        playOpenAnimationButton.addEventListener('click', () => {
            playOpenAnimation();
        });


        function sunMoving() {
            // start sun rotating animation after scene is loaded
            animations.bornInTheUSA()
            sun.visible = false;
            directionalLightTop.visible = false;
        }

        sunMovingButton.addEventListener('click', () => {
            sunMoving();
            //directionalLightTop.visible = false;
        })


        function animate() {
            mixer.update(clock.getDelta());
            renderer.render(scene, camera);
        }
        
        renderer.setAnimationLoop(animate);

        // Hide the progress bar after loading model
        setTimeout(() => {
            loadingBarContainer.style.display = 'none';
        }, 2000); 

    },
    (xhr) => {
        // This callback is called while the model is being loaded
        const percentLoaded = (xhr.loaded / xhr.total) * 100;
        updateLoadingProgress(percentLoaded); // Call your function to update UI
    }   
);


/**
 * Objects
 */
// Sun
const geometry = new THREE.SphereGeometry( 1, 32, 32 );
const material = new THREE.MeshBasicMaterial( { color: component.sun } );
const sun = new THREE.Mesh( geometry, material );
const scaleFactor = 0.5;
sun.scale.set(scaleFactor, scaleFactor, scaleFactor)
//sun.position.set(0, 0, 0)
//scene.add(sun);

// create object to focus the centere of the scene
const center = new THREE.Object3D();
center.position.set(0, 0, 0);
scene.add(center)

// spiral for sun animation
// Create line array 
const linePoints = [];
// Set the curve's points using a loop and add them to the geometry
for (var i = 0; i <= 200; i++) 
{   
    var angle = i * 0.15;
    var x = Math.cos(angle) * (2 + angle / 4);
    var y = i / 9;
    var z = Math.sin(angle) * (2 + angle / 4);
    linePoints.push(new THREE.Vector3(x, y, z))
}
const geometrySpiral = new THREE.BufferGeometry().setFromPoints( linePoints );
// Create a new LineBasicMaterial with a red color
var materialSpiral = new THREE.LineBasicMaterial({ color: 0xffff00 });
// Create a new Line object using the geometry and material
var line = new THREE.Line(geometrySpiral, materialSpiral);
// Add the line to the scene
//scene.add(line);


/**
 * Helpers
 */
const axesHelper3dViewer = new THREE.AxesHelper( 15 );
scene.add(axesHelper3dViewer);
axesHelper3dViewer.visible = false;


/**
 * Viewport settings
 */
const sizes = {
    width: window.innerWidth,
    height: window.innerHeight
}

window.addEventListener('resize', () =>
{
    // Update sizes
    sizes.width = window.innerWidth
    sizes.height = window.innerHeight

    // Update camera
    camera.aspect = sizes.width / sizes.height
    camera.updateProjectionMatrix()

    // Update renderer
    renderer.setSize(sizes.width, sizes.height)
    renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))
})


/**
 * Lights
 */
// Directional light
const directionalLightTop = new THREE.DirectionalLight(0xffffff, 2)
directionalLightTop.position.set(0, 10, 0)
scene.add(directionalLightTop);
const directionalLightTopHelper = new THREE.DirectionalLightHelper( directionalLightTop, 5 );
scene.add(directionalLightTopHelper);
directionalLightTopHelper.visible = false;

const directionalLightBottom = new THREE.DirectionalLight(0xffffff, 1)
directionalLightBottom.position.set(0, -5, 0)
scene.add(directionalLightBottom);
const directionalLightBottomHelper = new THREE.DirectionalLightHelper( directionalLightBottom, 5 );
scene.add(directionalLightBottomHelper);
directionalLightBottomHelper.visible = false;


// Point light
const pointLight = new THREE.PointLight( 0xffffff, 100, 0 );
pointLight.position.set(0, 8, 0);
// Point light shadow settings
pointLight.castShadow = true;
pointLight.shadow.mapSize.width = 2048
pointLight.shadow.mapSize.height = 2048
pointLight.shadow.camera.near = 0.5;    // Shadow start distance from light
pointLight.shadow.camera.far = 50;

pointLight.add(sun) // artifical sun 
scene.add(pointLight)

const pointLightHelper = new THREE.PointLightHelper(pointLight, 2 );
scene.add(pointLightHelper)
pointLightHelper.visible = false;


/**
 * Camera
 */
// Base camera
const camera = new THREE.PerspectiveCamera(75, sizes.width / sizes.height, 0.1, 100)
camera.position.set(3, 3, -3);
scene.add(camera)

// Controls
const controls = new OrbitControls(camera, canvas)
controls.enableDamping = true
controls.autoRotate = true
controls.autoRotateSpeed = 0.35
// OrbitControls Gizmo
const controlsGizmo = new  OrbitControlsGizmo(controls, { size: 150, padding: 8 });
// add Gizmo to DOM
document.body.appendChild(controlsGizmo.domElement)


/**
 * Renderer
 */
const renderer = new THREE.WebGLRenderer({
    canvas: canvas
})
renderer.setSize(sizes.width, sizes.height)
renderer.setPixelRatio(Math.min(window.devicePixelRatio, 2))
renderer.shadowMap.enabled = true


/**
 * Debuging
*/
// debug ui main component
const gui = new dat.GUI({width: 250}).title("Settings").close();
gui.domElement.style.position = 'absolute';
gui.domElement.style.left = '20px';
gui.domElement.style.display = 'block';

// add debug folder
//const debug = gui.addFolder('Debug').open();


// add scene adjustments for light sources folder
const sceneLightAdjustmentsFolder = gui.addFolder('Light Adjustments').close();
// add all light sources
const pointLightSourceFolder = sceneLightAdjustmentsFolder.addFolder('Sun').close();
pointLightSourceFolder.add(pointLight, 'visible');
pointLightSourceFolder.add(pointLight, 'intensity', 0, 100, 1)
const pointLightHelperFolder = pointLightSourceFolder.addFolder('Helper');
pointLightHelperFolder.add(pointLightHelper, 'visible');
const pointLightSourcePositionFolder = pointLightSourceFolder.addFolder('Move').close();
pointLightSourcePositionFolder.add(pointLight.position, 'x', -3, 10, 0.5)
pointLightSourcePositionFolder.add(pointLight.position, 'y', -3, 10, 0.5)
pointLightSourcePositionFolder.add(pointLight.position, 'z', -3, 10, 0.5)

const directionalLightSourceBottomFolder = sceneLightAdjustmentsFolder.addFolder('Bottom Light').close();
directionalLightSourceBottomFolder.add(directionalLightBottom, 'visible');
directionalLightSourceBottomFolder.add(directionalLightBottom, 'intensity', 0, 10, 0.01)
const directionalLightSourceBottomHelperFolder = directionalLightSourceBottomFolder.addFolder('Helper');
directionalLightSourceBottomHelperFolder.add(directionalLightBottomHelper, 'visible');

const directionalLightSourceBottomPosition = directionalLightSourceBottomFolder.addFolder('Move').close();
directionalLightSourceBottomPosition.add(directionalLightBottom.position, 'x', -3, 3, 0.5)
directionalLightSourceBottomPosition.add(directionalLightBottom.position, 'y', -3, 3, 0.5)
directionalLightSourceBottomPosition.add(directionalLightBottom.position, 'z', -3, 3, 0.5)

const directionalLightSourceTopFolder = sceneLightAdjustmentsFolder.addFolder('Top Light').close();
directionalLightSourceTopFolder.add(directionalLightTop, 'visible');
directionalLightSourceTopFolder.add(directionalLightBottom, 'intensity', 0, 10, 0.01)
const directionalLightSourceTopHelperFolder = directionalLightSourceTopFolder.addFolder('Helper');
directionalLightSourceTopHelperFolder.add(directionalLightTopHelper, 'visible');
const directionalLightSourceTopPosition = directionalLightSourceTopFolder.addFolder('Move').close();
directionalLightSourceTopPosition.add(directionalLightTop.position, 'x', -3, 3, 0.5)
directionalLightSourceTopPosition.add(directionalLightTop.position, 'y', -3, 3, 0.5)
directionalLightSourceTopPosition.add(directionalLightTop.position, 'z', -3, 3, 0.5)

/**
 * Scene specific functions
 */
// Change backround colour based on time like pseudo day 'n' night cycle
// set up day and night cycle colors
 const dayColor = new THREE.Color(0xcce0ff)
 const nightColor = new THREE.Color(0x222244)

// set initinal background color
 scene.background = dayColor
// function to update background color based on current time
const updateTime = () => {
    // get date and time of the moment
    const now = new Date()
    const startOfDay = new Date(now.getFullYear(), now.getMonth(), now.getDate())
    const totalSeconds = Math.floor((now - startOfDay) / 1000)  //- 50000
    const percentOfDay = (totalSeconds / 86400)  // 86400 seconds in a day
    // It's daytime, interpolate between day and night colors
    const percentToNight = percentOfDay // * 2
    const r = dayColor.r + (nightColor.r - dayColor.r) * percentToNight
    const g = dayColor.g + (nightColor.g - dayColor.g) * percentToNight
    const b = dayColor.b + (nightColor.b - dayColor.b) * percentToNight
    scene.background = new THREE.Color(r, g, b);
}
// update the time every second
setInterval(updateTime, 1000)


/**
 * Animate
*/
const clock = new THREE.Clock();
const tick = () =>
{
    const elapsedTime = clock.getElapsedTime()
    // Update controls
    controls.update()
    // Render
    renderer.render(scene, camera)
    // Call tick again on the next frame
    window.requestAnimationFrame(tick)
}
tick()


/**
 * Secret
 */
const e=['💪','🎓','🌟','🔥','🚀','✨','🎉','😄','🙌','🦁','👏','🥳','🌠'],s=["You're going to crush this PhD journey, Pia!","Pia, your hard work and perseverance inspire everyone around you!","You were made for great things, Pia—this PhD will be a piece of cake for you!","The world better be ready for Dr. Pia—you’re unstoppable!","Pia, every challenge you overcome is another step closer to greatness—keep going!","Your brilliance and determination shine so bright, Pia; nothing can stop you!","Proud of you, Pia! This PhD is just one of the many amazing things you'll achieve!","Keep going, Pia! You're already proving how amazing and capable you truly are!","Pia, remember: challenges are just stepping stones to your incredible success!","You're a force of nature, Pia—this PhD doesn’t stand a chance against you!"];function r(){return e[Math.floor(Math.random()*e.length)]}function m(){console.log(`${r()} ${s[Math.floor(Math.random()*s.length)]} ${r()}`)}m();

